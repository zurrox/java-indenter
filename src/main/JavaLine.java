package main;

public class JavaLine {
    private String line;

    public JavaLine(String line) {
        this.line = line;
    }

    /**
     *
     * @return whether there is a comment in the current line
     */
    public boolean hasComment() {
        return getCommentStart() >= 0;
    }

    /**
     *
     * @return the character of the start of the comment
     */
    public int getCommentStart() {
        int start, offset = 0; //Need offset as I'm cutting the String so the indexOf("//") is of the new cut String
        String tempStr = line;
        while(tempStr.contains("//")) {
            start = tempStr.indexOf("//");
            if(isInString(start)) {
                offset += start + 2;
                tempStr = tempStr.substring(start + 2);
            }else return start + offset;
        }
        return -1;
    }

    /**
     *
     * @return the content of the comment(Excluding the //)
     */
    public String getComment() {
        int commentLine = getCommentStart();
        if(commentLine >= 0) return line.substring(commentLine + 2);
        return null;
    }

    /**
     *
     * @return the code in the line excluding the comment without leading spaces and replaced tabs with spaces
     */
    public String getCode() {
        int commentLine = getCommentStart();
        String s = line;
        if(commentLine >= 0) s = line.substring(0, getCommentStart()); //Has Comment
        return removeLeadingSpaces(s.replaceAll("\t", "    ")); //Replace tabs(\t) with 4 spaces as \t = 1 char whereas should be 4
    }

    /**
     *
     * @return all of the text in the current line
     */
    public String getLine() {
        return line;
    }

    /**
     *
     * @param s - The string for the spaces to be removed from
     * @return the string after the leading spaces has been removed
     */
    private String removeLeadingSpaces(String s) {
        for(int i = s.length() - 1; i > 0; i--) {
            if(s.charAt(i) == ' ') s = s.substring(0, i);
            else return s;
        }
        return s;
    }

    /**
     *
     * @param line - The position in the line
     * @return Whether the character at the line position is in a String
     */
    private boolean isInString(int line) {
        int quotes = 0;
        if(line <= this.line.length()) {
            if((this.line.contains("\"") && this.line.indexOf('"') <= line)) {
                for(int i = 0; i < line; i++) {
                    if(this.line.charAt(i) == '"') quotes++;
                }
            }
        }
        return quotes > 0 && quotes % 2 != 0; //More than 0 and an odd number
    }

    /**
     * This method indents the comment to a specific character
     * @param line - The character for the comment to start on
     */
    public void indentComment(int line) {
        String code = getCode(), comment = getComment(), spaces = "";
        int spacesNeeded = line - code.length();
        for(int i = 0; i < spacesNeeded && hasComment(); i++) spaces += " ";
        this.line = code + (hasComment() ? spaces + "//" + comment : "");
    }
}