package main;

public class Main {
    public static JavaFile file;
    public static void main(String[] args) {
        String saveFile = null;
        if(args.length > 0) {
            if(args[0].contains(".java")) file = new JavaFile(args[0]);
            else{
                System.out.println("Input file invalid (Expected: .java file)");
                System.exit(2);
            }
        }else{
            System.out.println("Not enough arguments (Expected: A .java file)");
            System.exit(1);
        }

        if(args.length > 1 && args[1].contains(".java")) saveFile = args[1];

        file.indentComments();
        file.printProgram();

        if(saveFile != null) file.saveToFile(saveFile);
    }
}