package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class JavaFile {
    private ArrayList<JavaLine> lines;
    public JavaFile(String filePath) {
        readClass(filePath);
    }

    /**
     *  Reads the file and inserts the lines into the lines ArrayList
     * @param filePath - The path of the java file to be read
     */
    private void readClass(String filePath) {
        filePath = filePath.replace("\\", "\\\\");
        lines = new ArrayList<>();
        Path path = Paths.get(filePath);
        try {
            List<String> lines = Files.readAllLines(path);
            for(String s : lines) this.lines.add(new JavaLine(s));
        }catch(IOException e) {
            System.out.println("File not found");
            System.exit(3);
        }
    }

    /**
     *
     * @param line - The line number in the file
     * @return The contents of the line with the line number
     */
    public JavaLine getLine(int line) {
        if(lines.size() > line) return lines.get(line);
        return null;
    }

    /**
     *
     * @return The size of the longest line in the file
     */
    public int getLongestLine() {
        int max = 0;
        for(JavaLine line : lines) {
            if(line.getCode().length() > max) max = line.getCode().length();
        }
        return max;
    }

    /**
     *  Prints each line of the file to console
     */
    public void printProgram() {
        for(JavaLine line : lines) System.out.println(line.getLine());
    }

    /**
     *  Indents all comments to be at the same character
     */
    public void indentComments() {
        int longestLine = getLongestLine();
        for(int i = 0; i < lines.size(); i++) {
            JavaLine line = lines.get(i);
            line.indentComment(longestLine);
            lines.set(i, line);
        }
    }

    /**
     *
     * @param filePath - The path of the file to save the lines to
     */
    public void saveToFile(String filePath) {
        try{
            Path newFile = Paths.get(filePath);
            if(newFile.toFile().exists()) Files.delete(newFile);
            File file = newFile.toFile();
            if(!newFile.getParent().toFile().exists()) newFile.getParent().toFile().mkdirs();
            if(!newFile.toFile().exists()) newFile.toFile().createNewFile();
            FileWriter fw = new FileWriter(file);
            for(JavaLine line : lines) fw.write(line.getLine() + "\n");
            fw.flush();
            fw.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}